<?php
      class Login extends MY_Controller
      {
            function __construct(){
              parent::__construct();
              $this->load->model('M_login');
              $this->load->helper('form','html');
              $this->load->library('form_validation');
              $this->load->library('session');
            }

          function index() {

            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
                if ($this->form_validation->run() == FALSE) {
                      if(isset($this->session->userdata['logged_in'])){
                        redirect('index.php/beranda','refresh');

                          }else{

                        $this->load->view('page-login');
                      }

                      } else {
                        $pass=$this->input->post('password');
                        $data = array(
                            'username' => $this->input->post('username'),
                            'password' => MD5($pass)
                            );

                            $result = $this->M_login->login($data);
                            if ($result == TRUE) {

                              $username = $this->input->post('username');
                              $result = $this->M_login->read_user_information($username);
                                if ($result != false) {
                                      $session_data = array(
                                        'username' => $result[0]->username,
                                        'password' => $result[0]->password,
                                    );
          // Add user data in session

                  $this->session->set_userdata('logged_in', $session_data);
                redirect('index.php/beranda','refresh');
                }
                    } else {


                      $cetak="<div  class='alert alert-danger alert-dismissible fade in' ole='alert'>
                      <strong>error mas!</strong> Username karo password mu salah :P.
                      </div>";
                      echo $cetak;
                            $this->load->view('page-login', $data);
                            }
                          }
                        }


                  // fungsi logout / keluar
                  function logout()
                  {
                    $sess_array  = array(
                      'username' => ''
                    );
                    $this->session->unset_userdata('logged_in',$sess_array);
                    redirect('index.php/template','refresh');
                  }


                  // fungsi untuk menambah user ke database
                  	function add_user(){
                  			$this->form_validation->set_rules('username', 'username anda sudah ada', 'required|is_unique[login.username]|min_length[1]');
                        $this->form_validation->set_rules('email', 'email anda sudah ada', 'required|is_unique[login.email]|min_length[1]');
                  			;
                  		if ($this->form_validation->run() == FALSE)
                  		{
                  			$this->load->view('page-register');
                        $cetak="<div  class='alert alert-danger alert-dismissible fade in' ole='alert'>
                        <strong>error mas!</strong> Username mu wes ono :P.
                        </div>";
                        echo $cetak;
                  		}
                  		else {
                  			$req = $this->input->post('proses');
                  	 		switch ($req) {
                  	 			case 'simpan':
                  	 				# simpan ke database
                  	 				$this->M_login->post_data_regsiter();
                  	 				$data = array('' => '' );
                  	 				$this->load->view('page-login', $data);
                  	 				break;

                  	 			default:
                  	 				$data = array('' => '' );
                  	 				$this->load->view('page-login', $data);
                  	 				break;
                  	 		}
                  		}
                  	}

                    function register() {
                      $this->load->view('page-register');
                    }

                    function forgot_password() {
                      $this->load->view('page-forgot-password');
                    }



}
